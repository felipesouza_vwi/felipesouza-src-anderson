<section class="w-100 hero-top-primary position-absolute">
    <div class="container">
        <div class="col-lg-12">
            <div class="row">
                <div class="w-100 color-header">
                    <div class="row">
                        <div class="col-lg-3">
                            <img src="https://demo.voidcoders.com/htmldemo/potoliaV2/main-files/assets/img/logo-1.png"
                                 class="mt-3 ml-2" alt="">
                        </div>
                        <div class="col-lg-6"></div>
                        <div class="col-lg-3"></div>
                    </div>
                </div>
            </div>
        </div>
</section>
<section class="w-100 position-absolute hero-top">
    <div class="container">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <h2 class="hero-text-h2 m-auto">Photography Is An Immediate
                            Reaction,
                            <span class="d-flex justify-content-center">Drawing Is A Meditation Props.</span></h2>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="row">
                        <p class="hero-text-p">Photography is a way of feeling, of touching, of loving. What you have
                            caught on film is
                            captured forever... it <span class="d-flex justify-content-center">remembers little things, long after you have forgotten
                            everything.</span></p>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="row">
                        <div class="m-auto">
                            <div class="btn hero-btn">
                                <p class="mt-2">CONTACT US</p>
                            </div>
                            <div class="btn hero-btn-secondary ml-3">
                                <p class="mt-2">PORTIFOLIO</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="row">
                        <div class="m-auto">
                            <div class="mt-5">
                                <a href="#hero-photo">
                                    <i class="fa fa-angle-double-down hero-icon" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>